# Project Euler 742

<p>A <i>symmetrical convex grid polygon</i> is a polygon such that:</p>
<ul>
<li>All its vertices have integer coordinates.</li>
<li>All its internal angles are strictly smaller than 180°.</li>
<li>It has both horizontal and vertical symmetry.</li>
</ul>

For example, the left polygon is a convex grid polygon which has neither horizontal nor vertical symmetry, while the right one is a valid symmetrical convex grid polygon with six vertices:
<div style="text-align:center;">
<img src="https://projecteuler.net/project/images/p742_hexagons.jpg" class="dark_img" alt="" /></div>

Define $`A(N)`$, the minimum area of a symmetrical convex grid polygon with $`N`$ vertices.

You are given $`A(4) = 1`$, $`A(8) = 7`$, $`A(40) = 1039`$ and $`A(100) = 17473`$.

Find $`A(1000)`$.

# Symmetry

Since each polygon is symmetrical along the horizontal and vertical axis, dividing the polygon into quadrants yields identical pieces. If the amount of edges is given as $`N`$ then the amount of edges in each quadrant is $`\frac{N}{4}`$ .

The result is awkward for $`N`$ such that $`Nmod4 \neq 0`$; 6 for example yields 1.5 edges per quadrant.

The solution is to consider the angle that this half edge enters our quadrant at. If the angle $`\alpha`$ between the y-axis and the edge is less or greater than 90°, and we know that it has to continue on the other side of the y-axis at an angle of 180° - \alpha, which for any angle other than 90° would make our quadrants non-symmetrical.

Furthermore, if we can not adjust the angle of the incoming half edge, then there's no way to reduce the area underneath it, other than by adjusting the line between the y-axis and the node connecting the next edge. Therefore we can ignore the half edge and consider just the shift of the node connecting the next edge; adding a correction term of $`Nmod4`$ for our area.

# Continuous interpretation

If, instead of viewing the line in the quadrant as simply the perrimiter of our polygon, we consider it the continuous path of a particle interacting with a very peculiar vector field, we can expand this discrete problem into a continuous one.

We make the following observations about this particle

- Firstly: the function representing our particle's path must always be concave down.
- Secondly: the area under the function -which is what we are trying to minimize- must converge.
- Thirdly: Our starting point is limited to the boundry condition x = 0 and our ending point is limited to the boundry condition y = 0.

Parameterizing the x and y components we note that the y components must always be decreasing over some parameter, call it t, whilst the x component must always be increasing over t.

# Acceleration and Mass

I initially fell into the non-homotopic trap and set my initial conditions for concavity as the following $`\frac{d^2y}{dt^2} < 0`$
and $`\frac{d^2x}{dt^2} > 0`$.

However, these conditions can be represented as homotopic constraints in the following way $`\bar{\frac{d^2y}{dt^2}} = -1`$ and
$`\bar{\frac{d^2x}{dt^2}} = 1`$, respectively.

If we consider our acceleration along this path as $`\vec{\frac{d^2r}{dt^2}}=\begin{bmatrix} \frac{d^2x}{dt^2} \\ \frac{d^2y}{dt^2} \end{bmatrix}`$, and take mass as 1, then the right hand side of Newton's familiar second law yields $`\begin{bmatrix} \frac{d^2x}{dt^2} \\ \frac{d^2y}{dt^2} \end{bmatrix}`$.

# Force

At the most general level, from Helmholtz's theorem, we could consider our vector field as being represented by the composition of an irrotational and solenoidal component. If it is assumed that the amount of time between each integer pair is 1, then velocity becomes directly proportional to the distance traveled, and we have to assume that the distance between each node is equal. A quick mental application of the Pythagorean theorem rejects this idea; requiring our vector field to have a non-zero divergence.

If instead we relax our definition of time, and suggest that our particle may take a liesurely stroll or a sprint at mach 1 through the vector field then our distance is proportional to velocity up to some constant of the interval of time between each point. As long as our $`\delta t`$ can be any real number the magnitude of our vector field doesn't need to change and can be represented as a solenoidal vector field. A solenoidal vector field $`\vec{F}`$ can be represented as $`\vec{F}= \vec{\nabla} \times \vec{A} `$.

The only issue, is that our niave particle will be tempted by the vector field to follow a curvy path. The solution is to introduce an impulse train of period 1, that creates a window to our vector field at each integer pair. The impulse train in two dimensions is known as a dirac comb, in three dimensions I shall refer to it as a dirac-combover.

An important note is that I am not refering to a delta distribution, who's area is 1, width is dx, and who's height is infinity. I am instead, refering to a distribution with an infentisimal width and a height of 1. I also use function, impulse train, and distribution interchangably, which I understand may cause an annurysum for some mathematicians: and I applogize in advance.

To denote this distribution one would, by convention, use a sideways E usually called shaw, after it's resembelance to the Hebrew character. Due to the limitations of KaTeX -the GitLab implementation of LaTeX- I will use capital lambda, for no other reason than I think it looks cool.

Finally, we arrive at an expression of the force: $`\Lambda_{(x,y)}(\vec{\nabla} \times \vec{A})`$

Combining our expression of force and acceleration, with Newton's law, we arrive at: $`\Lambda_{(x,y)}(\vec{\nabla} \times \vec{A}) = \begin{bmatrix} \frac{d^2x}{dt^2} \\ \frac{d^2y}{dt^2} \end{bmatrix}`$

# Laplace Transform

The laplace transform of the right hand side of Newton's law is tricky: I had to look at the back of the laplace transform table to find it. $`\mathscr{L} \begin{Bmatrix} \begin{bmatrix} \frac{d^2x}{dt^2} \\ \frac{d^2y}{dt^2} \end{bmatrix} \end{Bmatrix}  = \begin{bmatrix} S^2X(s) \\ Sy_0 -S^2Y(s) \end{bmatrix}`$

The left side proves more challenging; the first step is to remember that the window function $`\Lambda_{(x,y)}`$ is really a sum of individual impulses. By the linearity of the Laplace transform, we can move it within the double summation:     

$`\mathscr{L} \begin{Bmatrix} \Lambda_{(x,y)}(\vec{\nabla} \times \vec{A}) \end{Bmatrix} = \sum_{n = 0}^{g} \sum_{i = 0}^{g} \mathscr{L} \begin{Bmatrix} (\vec{\nabla} \times \vec{A}) \bar{\delta}(x-i) \bar{\delta}(y-n) \end{Bmatrix} = \sum_{n = 0}^{g} \sum_{i = 0}^{g} (\vec{\nabla} \times \vec{A})|_{x = i, y = n} \mathscr{L} \begin{Bmatrix} \bar{\delta}(x-i) \bar{\delta}(y-n) \end{Bmatrix}`$.

I chose to denote the unit impulse spikes as $`\bar{\delta}(x-i)`$ which is the distribution centered at i on the x-axis. Evaluating the laplace transform of the product of is a confusing task $`\bar{\delta}(x-i) \bar{\delta}(y-n)`$. It's also helpful to remember that the x and y inside the distribution are refering to the x and y component of our particles position, which is dependent on t.

When I previously did this problem, incorrectly assuming I could represent my force as the gradient of a scalar field the solution popped out easily with convolution.

$`\mathscr{L} \begin{Bmatrix} \bar{\delta}(x-i) \bar{\delta}(y-n) \end{Bmatrix} = \int_{0}^{\infty}e^{-st}\bar{\delta}(x-i) \bar{\delta}(y-n)dt`$

Each distribution can be expanded via a Fourier series:

<iframe src="https://www.desmos.com/calculator/uuowir2xfy?embed" width="500px" height="500px" style="border: 1px solid #ccc" frameborder=0></iframe>
